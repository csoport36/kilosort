spike_clusters = readNPY('Panna/spike_clusters.npy'); 
channel_positions= readNPY(['Panna/channel_positions.npy']);
channel_map=readNPY(['Panna/channel_map.npy']);
templates_unw=readNPY(['Panna/templates_unw.npy']);
pc_feature_ind=readNPY(['Panna/pc_feature_ind.npy']);
pc_features=readNPY(['Panna/pc_features.npy']);
similar_templates=readNPY(['Panna/similar_templates.npy']);
amplitudes=readNPY(['Panna/amplitudes.npy']);
spike_templates=readNPY(['Panna/spike_templates.npy']);
spike_times=readNPY(['Panna/spike_times.npy']);
template_feature_ind=readNPY(['Panna/template_feature_ind.npy']);
template_features=readNPY(['Panna/template_features.npy']);
templates=readNPY(['Panna/templates.npy']);
templates_ind=readNPY(['Panna/templates_ind.npy']);
whitening_mat=readNPY(['Panna/whitening_mat.npy']);
whitening_mat_inv=readNPY(['Panna/whitening_mat_inv.npy']);

